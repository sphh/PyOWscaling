#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `Schoenherr` implementing the `IFrictionlinePlugin` interface for full
scale.

Plug-ins:
    Schoenherr:
        Frictional coefficient according to Schönherr's implicite friction
        line.


Created on Tue Feb  7 14:33:12 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from numpy import log10, sqrt
import scipy.optimize

from owscaling.plugins import IFrictionlinePlugin


class Schoenherr(IFrictionlinePlugin):
    """Frictional coefficient (full scale) according to Schönherr's implicite
    friction line.

    Source:
        Schönherr, K.E.:
            ‘Resistance of Flat Surfaces Moving Through a Fluid’.
            SNAME Transactions, 1932, Vol. 40, pp.279.
    """
    name = 'Schönherr'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def __call__(self, RN, **kwargs):
        """Calculate the friction coefficient of a section.

        Parameters:
            RN:
                The Reynolds number based on the section chord length [-].

        Return:
            The frictional coefficient [-].

        Exceptions:
            scipy.optimize.nonlin.NoConvergence:
                The solver for the Schönherr line does not converge.
        """

        # Conditional equation for cF, which must be 0
        func = lambda cF, RN: log10(RN*cF) - 0.242/sqrt(cF)

        # 1st derivative of `func`
        fprime = lambda cF, RN: 0.121/cF**(3/2) + 0.434294/cF

        # 2nd derivative of `func`
        fprime2 = lambda cF, RN: -0.1815/cF**(5/2) - 0.434294/cF**2

        # Starting point (using ITTC's frictional coefficient)
        cF0 = 0.075 / (log10(RN) - 2)**2

        cF = scipy.optimize.newton(
                func, cF0,
                fprime=fprime, fprime2=fprime2,
                args=(RN,))

        if abs(func(cF, RN)) <= 1.48e-08:
            return cF
        else:
            raise scipy.optimize.NoConvergence(
                    'The friction coefficient found is not a root of the '
                    'conditional equation: f({}) = {}'.format(cF, func(cF, RN)))


if __name__ == '__main__':

    fl = Schoenherr({})
    print(fl)

    RN = 1e9            # Reynolds number [-]

    print('Friction coefficient for')
    print('    RN={:g}'.format(RN))
    print('ITTC 1957: ', 0.075 / (log10(RN)-2)**2)
    print('Schoenherr:', fl(RN))
