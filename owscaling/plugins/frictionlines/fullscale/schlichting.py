#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `Schlichting` implementing the `IFrictionlinePlugin` interface for
full scale.

Plug-ins:
    Schlichting:
        Frictional coefficient according to the local skin friction
        coefficient mentioned in Schlichting/Gersten.


Created on Fri Feb  3 14:12:29 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from numpy import log10, sqrt, log
import scipy.optimize

from owscaling.plugins import IFrictionlinePlugin, autoargs


ln = log                    # np.log: Natural logarithm
ln_34 = ln(3.4)
sqrt_2_34 = 3.4*sqrt(2)


class Schlichting(IFrictionlinePlugin):
    """Frictional coefficient (full scale) according to the local skin friction
    coefficient mentioned in Schlichting/Gersten.

    The original idea by Schlichting was slightly modified by Heinrich
    Streckwall and Stephan Helma.

    Source:
        Schlichting, H. & Gersten, K.:
            Grenzschicht-Theorie.
            Berlin, Germany, 2006: Springer Verlag.
    """
    name = 'Schlichting'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(
            self,
            kappa: {
                'args': '--karman',
                'type': float,
                'help': 'von Kármán constant ϰ'
                } = 0.41,
            KP: {
                'args': '--kp',
                'type': float,
                'help': 'roughness of the (full scale) propeller blade [m]'
                } = 2e-5,
            *args, **kwargs):
        super().arguments(*args, **kwargs)


    def __str__(self):
        return (super().__str__() +
        """ϰ:          {self.args.kappa}
        kp:         {self.args.KP}
        """.format(self=self))

    def __call__(self, RN, LCH_x=None, **kwargs):
        """Calculate the friction coefficient of a section.

        Parameters:
            RN:
                The Reynolds number based on the section chord length [-].
            LCH_x:
                The chord length of the profile [m].

        Return:
            The frictional coefficient [-].

        Exceptions:
            scipy.optimize.nonlin.NoConvergence:
                The solver for the Schlichting line does not converge.
        """

        # Conditional equation for cF, which must be 0
        # Note:
        # We multiplied the original equation
        #    func = lambda cF, RN, ϰ, xkc: (
        #            1 / ϰ * ln(cF/2 * RN)
        #            + 2 + ln_34/ϰ
        #            - 1 / ϰ * ln(3.4 + sqrt(cF/2 * xkc * RN))
        #            - sqrt(2/cF))
        # by ϰ to improve convergence
        func = lambda cF, RN, kappa, xkc: (
                ln(cF/2 * RN)
                + 2*kappa + ln_34
                - ln(3.4 + sqrt(cF/2) * xkc * RN)
                - kappa*sqrt(2/cF))

        # 1st derivative of original equation
        #    fprime = lambda cF, Rn, ϰ, xkc: (
        #            1 / (ϰ * cF)
        #            - xkc * RN / (ϰ * 2 * (sqrt_2_34 * sqrt(cF) + cF*xkc*RN))
        #            + 1 / sqrt(2 * cF**3))
        fprime = lambda cF, Rn, kappa, xkc: (
                1 / cF
                - xkc * RN / (2 * (sqrt_2_34 * sqrt(cF) + cF*xkc*RN))
                + kappa / sqrt(2 * cF**3))

        # 2nd derivative of original equation
        #    fprime2 = lambda cF, RN, ϰ, xkc: (
        #            -1 / (ϰ * cF**2)
        #            + 1 / ϰ * (
        #                    (sqrt_2_34 + 2*sqrt(cF)*xkc*RN) * xkc * RN
        #                    / (4 * sqrt(cF**3)
        #                    * (sqrt_2_34 + sqrt(cF)*xkc*RN)**2))
        #            - 3 / (2 * sqrt(2 * cF**5)))
        fprime2 = lambda cF, RN, kappa, xkc: (
                -1 / cF**2
                + ((sqrt_2_34 + 2*sqrt(cF)*xkc*RN)
                    * xkc
                    * RN
                    / (4 * sqrt(cF**3)
                    * (sqrt_2_34 + sqrt(cF)*xkc*RN)**2))
                - 3*kappa / (2 * sqrt(2 * cF**5)))

        # Starting point (using ITTC's frictional coefficient)
        cF0 = 0.075 * (1 - 0.869/(log10(RN) - 2)) * (log10(RN) - 2)**(-2)

        # Roughness to chord length ratio
        xkc = self.args.KP/LCH_x

        cF = scipy.optimize.newton(
                func, cF0,
                fprime=fprime, fprime2=fprime2,
                args=(RN, self.args.kappa, xkc),
                maxiter=1000)

        if abs(func(cF, RN, self.args.kappa, xkc)) <= 1.48e-08:
            return cF
        else:
            raise scipy.optimize.nonlin.NoConvergence(
                    'The friction coefficient found is not a root of the '
                    'conditional equation: f({}) = {}'.format(
                            cF, func(cF, RN, self.args.kappa, xkc)))


if __name__ == '__main__':

    fl = Schlichting({})
    print(fl)

    RN = 1e9            # Reynolds number [-]
    XKPLCH = 0.00001    # Ratio roughness to section length [-]

    LCH = fl.KP/XKPLCH  # Section length [m]

    f1 = '{:<6} {:<14} {:<14} {:<10} {:<10}'
    f = '{:<6.0e} {:<14.4} {:<14.4} {:<10.4} {:<10.4}'
    print('Friction coefficient for')
    print('    LCH={}m'.format(LCH))
    print('    k={:g}m'.format(fl.KP))
    print('    k/LCH={:g}'.format(XKPLCH))
    print(f1.format('RN', 'ITTC1978 ship', 'ITTC1978 m', 'ITTC1957', 'Schlichting'))
    for i in range(4,11):
        RN = 10**i
        print(f.format(
                RN,
                (1.89 + 1.62*log10(LCH / fl.KP))**(-2.5),
                0.044/RN**(1/6) - 5/RN**(2/3),
                0.075 / (log10(RN)-2)**2,
                fl(RN, LCH_x=LCH)))
