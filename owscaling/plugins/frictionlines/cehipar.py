#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `CEHIPAR` implementing the `IFrictionlinePlugin` interface for model
scale.

Plug-ins:
    CEHIPAR:
        Frictional coefficient as suggested by CEHIPAR in 2016.


Created on Fri Jan 27 10:12:23 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from numpy import log10, sqrt

from owscaling.plugins import IFrictionlinePlugin, autoargs
from owscaling.helpers import LimitedInt


class CEHIPAR(IFrictionlinePlugin):
    """Frictional coefficient (model and full scale) as suggested by CEHIPAR.

    Source:
        Ramón Quereda, Cristina Soriano, Mariano Pérez-Sobrino, Juan
        González-Adalid, Amadeo Morán and Giulio Gennaro (2016):
            ‘Scale effectes in open water test results for performance
            prediction of conventional and unconventional propellers’.
            Canal de Experiencas Hidrodinámicas de El Pardo (CEHIPAR),
            Publicacion núm. 215, April 2016.
    """
    name = 'CEHIPAR'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(
            self,
            KP: {
                'args': '--kp',
                'type': float,
                'help': 'roughness of the (full scale) propeller blade [m]'
                } = 2e-5,
            r_lam: {
                'args': ('-l', '--rlam'),
                'type': LimitedInt(min=0),
                'help': 'Factor to calculate the critical Reynoldsnumber below'
                        'which the flow will always be laminar'
                } = 90,
            r_turb: {
                'args': ('-t', '--rturb'),
                'type': LimitedInt(min=0),
                'help': 'Factor to calculate the critical Reynoldsnumber above'
                        'which the flow can be considered fully turbulent'
                } = 415,
            *args, **kwargs):
        super().arguments(*args, **kwargs)


    def __str__(self):
        return (super().__str__() +
        """kp:         {self.args.KP}
        """.format(self=self))

    def __call__(self, RN, LCH_x=None, **kwargs):
        """Calculate the friction coefficient of a section.

        Parameters:
            RN:
                The Reynolds number based on the section chord length [-].
            LCH_x:
                The chord length of the profile [m].

        Return:
            The frictional coefficient [-].
        """

        def CF_lam(RN):
            """Friction of fully laminar flow."""
            return 1.328 / sqrt(RN)

        def CF_turb(RN):
            """Friction of fully turbulent flow (Prandtl-Schlichting)."""
            # TODO:
            # Make Prandtl/Schlichting line configurable!
            return 0.455 / log10(RN)**2.58

        # Critical Reynolds number (laminar to transition)
        RN_lam2trans = self.args.r_lam * LCH_x / self.args.KP
        # Critical Reynolds number (transition to turbulent)
        RN_trans2turb = self.args.r_turb * LCH_x / self.args.KP

        if RN <= RN_lam2trans:
            # Laminar flow
            return CF_lam(RN)
        elif RN >= RN_trans2turb:
            # "Fully" turbulent flow
            return CF_turb(RN)
        else:
            # Transitional flow - linear interpolation
            CF_lam2trans = CF_lam(RN_lam2trans)
            CF_trans2turb = CF_turb(RN_trans2turb)
            return (CF_lam2trans
                    + (RN - RN_lam2trans)
                      / (RN_trans2turb - RN_lam2trans)
                      * (CF_trans2turb - CF_lam2trans))
