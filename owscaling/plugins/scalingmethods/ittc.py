#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `ITTC` implementing the `IScalingPlugin` interface.

Plug-ins:
    ITTC:
        The ITTC 1978 propeller scaling method.


Created on Thu Jan 26 14:34:48 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from numpy import pi, sqrt

from owscaling.plugins import IScalingPlugin


class ITTC(IScalingPlugin):
    """The ITTC 1978 propeller scaling method.

    Source:
        ITTC, Propulsion Committee of 27th ITTC 2014:
            ‘1978 ITTC Performance Prediction Method’.
            ITTC – Recommended Procedures and Guidelines, 1978, Effective Date
            2014, Revision 03.
    """
    name = 'ITTC'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def _scale(self):
        """Scale the open-water data."""

        #
        # Some shortcuts
        #

        p = self.propeller      # Propeller
        ow = self.openwater     # Open-water data

        XE = self.args.XE       # Fractional radius of equivalent profile
        NPB = p.NPB             # Number of propeller blades

        # Propeller diameters ...
        DP_ow = self.DP['ow']   # ... open-water
        DP_sp = self.DP['sp']   # ... self-propulsion
        DP_fs = self.DP['fs']   # ... full scale

        # Propeller shaft revs ...
        N_ow = self.N['ow']     # ... open-water
        N_sp = self.N['sp']     # ... self-propulsion
        N_fs = self.N['fs']     # ... full scale

        #
        # Store some common values for open-water, self-propulsion and full
        # scale
        #

        # Chord lengths of equivalent profile
        LCH_ow = p.chr(XE) * DP_ow/2
        LCH_sp = p.chr(XE) * DP_sp/2
        LCH_fs = p.chr(XE) * DP_fs/2

        # Pitch to diameter ratio
        PDR = p.pdr(XE)

        # Chord to diameter ratio
        CDR = p.chr(XE) / 2

        # Kinematic viscosities of tank and sea water
        VK_ow = self.viscosity['ow'](self.TEWA['ow'])
        VK_sp = self.viscosity['sp'](self.TEWA['sp'])
        VK_fs = self.viscosity['fs'](self.TEWA['fs'])

        #
        # Shortcut to functions of JEI
        #

        # Speeds of attack
        V_ow = lambda JEI: N_ow(JEI) * DP_ow * sqrt(JEI**2 + (pi*XE)**2)
        V_sp = lambda JEI: N_sp * DP_sp * sqrt(JEI**2 + (pi*XE)**2)
        V_fs = lambda JEI: N_fs * DP_fs * sqrt(JEI**2 + (pi*XE)**2)

        # Reynolds numbers
        RN_ow = lambda JEI: V_ow(JEI) * LCH_ow / VK_ow
        RN_sp = lambda JEI: V_sp(JEI) * LCH_sp / VK_sp
        RN_fs = lambda JEI: V_fs(JEI) * LCH_fs / VK_fs

        #
        # Do the scaling
        #

        # CD = 2 · CD2d · CF
        # ΔCD = CDm - CDs
        Delta_CD_sp = lambda JEI: (
                self.CD('ow', RN_ow(JEI), XE, DP_ow, JEI=JEI) -
                self.CD('sp', RN_sp(JEI), XE, DP_ow, JEI=JEI))
        Delta_CD_fs = lambda JEI: (
                self.CD('ow', RN_ow(JEI), XE, DP_ow, JEI=JEI) -
                self.CD('fs', RN_fs(JEI), XE, DP_fs, JEI=JEI))

        # ΔKT = -ΔCD · 0.3  · P/D · C/D · Z
        # ΔKQ =  ΔCD · 0.25 ·       C/D · Z
        Delta_KT_sp = lambda JEI: -Delta_CD_sp(JEI) * 0.3  * PDR * CDR * NPB
        Delta_KQ_sp = lambda JEI:  Delta_CD_sp(JEI) * 0.25       * CDR * NPB

        Delta_KT_fs = lambda JEI: -Delta_CD_fs(JEI) * 0.3  * PDR * CDR * NPB
        Delta_KQ_fs = lambda JEI:  Delta_CD_fs(JEI) * 0.25       * CDR * NPB

        #
        # Store the scaled KT and KQ curves as required
        #

        KT_ow = self.KT_ow
        KQ_ow = self.KQ_ow
        if self.scale_to['sp']:
            self.KT_sp = lambda JEI: KT_ow(JEI) - Delta_KT_sp(JEI)
            self.KQ_sp = lambda JEI: KQ_ow(JEI) - Delta_KQ_sp(JEI)
        else:
            self.KT_sp = KT_ow
            self.KQ_sp = KQ_ow
        if self.scale_to['fs']:
            self.KT_fs = lambda JEI: KT_ow(JEI) - Delta_KT_fs(JEI)
            self.KQ_fs = lambda JEI: KQ_ow(JEI) - Delta_KQ_fs(JEI)
        else:
            self.KT_fs = KT_ow
            self.KQ_fs = KQ_ow
