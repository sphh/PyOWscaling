#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `Dummy` implementing the `IScalingPlugin` interface.

Plug-ins:
    Dummy:
        A scaling method doing no scaling at all.


Created on Tue Jan 31 16:00:19 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from owscaling.plugins import IScalingPlugin


class Dummy(IScalingPlugin):
    """A scaling method doing no scaling at all."""
    name = 'dummy'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def _scale(self):
        """Scale the open-water data."""

        # A dummy scaling method does not change the open-water curves
        self.KT_sp = self.KT_ow
        self.KQ_sp = self.KQ_ow
        self.KT_fs = self.KT_ow
        self.KQ_fs = self.KQ_ow
