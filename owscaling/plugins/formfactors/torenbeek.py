#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `Torenbeek` implementing the `IFormfactorPlugin` interface.

Plug-ins:
    Torenbeek:
        Form factor according to Torenbeek.


Created on Sun Feb  5 21:39:21 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from owscaling.plugins import IFormfactorPlugin


class Torenbeek(IFormfactorPlugin):
    """Form factor according to Torenbeek.

    Source:
        Torenbeek, E.:
            ‘Synthesis of Subsonic Airplane Design’.
            The Netherlands and Martinus Nijhoff, Delft Univ. Press.
            Delft, The Netherlands, 1992.
    """
    name = 'Torenbeek'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def __call__(self, XTC, **kwargs):
        """Calculate the form factor of a section profile with thickness `XTC`.

        Parameters:
            XTC:
                The thickness to chord length ratio [-].

        Return:
            The form factor [-].
        """

        return 1 + 2.7*XTC + 100*XTC**4
