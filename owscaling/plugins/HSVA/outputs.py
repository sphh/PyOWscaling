#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-ins `HSVAxxx` implementing the `IOWWriterPlugin` interface.

Plug-ins:
    HSVAcsv:
        Write open-water data in HSVA's '*.csv' format.
    HSVAypp:
        Write open-water data in HSVA's '*.ypp' format.


Created on Wed Feb  1 15:27:32 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np

from owscaling.plugins import IOWWriterPlugin
from owscaling.helpers import FortranPrinter


class HSVAcsv(IOWWriterPlugin):
    """Write open-water data in HSVA's '*.csv' format.

    For the format definition see HSVA's documentation or the example file
    supplied.
    """
    name = 'HSVA.csv'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def _write(self, f):

        scaling = self.scaling
        propeller = scaling.propeller
        openwater = scaling.openwater

        # Get the range of propeller revs
        N_ow = scaling.N['ow']
        N_ow_data = [N_ow(JEI) for JEI in openwater.JEI_data]
        N_ow_min = min(N_ow_data)
        N_ow_max = max(N_ow_data)
        if N_ow_min == N_ow_max:
            N_ow_range = '{}'.format(N_ow_min)
        else:
            N_ow_range = '{}-{}'.format(N_ow_min, N_ow_max)

        p = FortranPrinter(f)
        p(openwater.get_filename())
        p(propeller.get_filename())
        p('{:<20};  ! Hull Num;', propeller.hullid)
        p('{:<20};  ! Prop-Number;', propeller.id)
        p('{:<20};  ! OW-Number;', openwater.id)
        p('{:<20};  ! D-Mod [m];', scaling.DP['ow'])
        p("{:<20};  ! '0':right/'1':left';", propeller.hand)
        p('{:<20};  ! like -2°;', openwater.deltaP)
        p('{:<20};  ! OW frequency;', N_ow_range)
        p('{:<20};  ! PT frequency;', scaling.N['sp'])
        p('{:<20};  ! FS_frequency [Hz];', scaling.N['fs'])
        p('{:<20};  ! T-Tank [°];', scaling.TEWA['sp'])
        p('{:<20};  ! T_OW [°];', scaling.TEWA['ow'])
        p('{:<20};  ! lambda;', scaling.SC)
        p()
        p('Relevant Output Data [Including Duct]:')
        p(' {:>9};'*10 + ';;;',
          'J',
          'KT_OW', '10KQ_OW',
          'KT_RP', '10KQ_RP',
          'KT_FS', '10KQ_FS',
          'KT_DOW', 'KT_DRP', 'KT_DFS')
        for JEI in openwater.JEI_data:
            p(' {:>9.4f};'*10 + ';;;',
              JEI,
              scaling.KT_ow(JEI), 10*scaling.KQ_ow(JEI),
              scaling.KT_sp(JEI), 10*scaling.KQ_sp(JEI),
              scaling.KT_fs(JEI), 10*scaling.KQ_fs(JEI),
              0, 0, 0)
        p('Last parameter for Friction Line control: {}', None)


class HSVAypp(IOWWriterPlugin):
    """Write open-water data in HSVA's '*.ypp' format.

    For the format definition see HSVA's documentation or the example file
    supplied.
    """
    name = 'HSVA.ypp'
    __version__ = '3.1.0'
    __copyright__ = '2017 Stephan Helma'


    def _write(self, f):

        scaling = self.scaling
        propeller = scaling.propeller
        openwater = scaling.openwater
        viscosity_ow = scaling.viscosity['ow']
        viscosity_sp = scaling.viscosity['sp']
        viscosity_fs = scaling.viscosity['fs']
        friction_ow = scaling.friction['ow']
        friction_sp = scaling.friction['sp']
        friction_fs = scaling.friction['fs']

        # Get the range of propeller revs
        N_ow = scaling.N['ow']
        N_ow_data = [N_ow(JEI) for JEI in openwater.JEI_data]
        N_ow_min = min(N_ow_data)
        N_ow_max = max(N_ow_data)

        p = FortranPrinter(f)
        p(' {:<8}{:<4}        ! Propeller number', propeller.id, 0)
        p(' {:<10}          ! arbitrary string since hull No. not requested in INPUT', 'blabla')
        p(' {:<9.7f}           ! Dmod', scaling.DP['ow'])
        p(' {:<9.7f} 0         ! Pm/D', propeller.PDR)
        p(' {:<9.7f}           ! rh/R', propeller.XBDR)
        p(' {:<9.7f}           ! Ae/A0', propeller.ADE)
        p(' {:>10}          ! Z', propeller.NPB)
        p(' {:<10}          ! direction of rotation (German)', 'LINKS' if propeller.hand else 'RECHTS')
        p(' {:<10}          ! direction of rotation (English)', 'LEFT' if propeller.hand else 'RIGHT')
        p()
        J_max = max(openwater.JEI_data)
        for i, JEI in enumerate(np.linspace(0, 2.1, 6*7)):
            p('  {:7.5f} ', scaling.KT_ow(JEI) if JEI<=J_max else 0., end='')
            if i%7 == 6:
                p()
        p()
        for i, JEI in enumerate(np.linspace(0, 2.1, 6*7)):
            p('  {:7.5f} ', 10*scaling.KQ_ow(JEI) if JEI<=J_max else 0., end='')
            if i%7 == 6:
                p()
        p()
        for i, JEI in enumerate(np.linspace(0, 2.1, 6*7)):
            p('  {:7.5f} ', scaling.KT_sp(JEI) if JEI<=J_max else 0., end='')
            if i%7 == 6:
                p()
        p()
        for i, JEI in enumerate(np.linspace(0, 2.1, 6*7)):
            p('  {:7.5f} ', 10*scaling.KQ_sp(JEI) if JEI<=J_max else 0., end='')
            if i%7 == 6:
                p()
        p()
        for i, JEI in enumerate(np.linspace(0, 2.1, 6*7)):
            p('  {:7.5f} ', scaling.KT_fs(JEI) if JEI<=J_max else 0., end='')
            if i%7 == 6:
                p()
        p()
        for i, JEI in enumerate(np.linspace(0, 2.1, 6*7)):
            p('  {:7.5f} ', 10*scaling.KQ_fs(JEI) if JEI<=J_max else 0., end='')
            if i%7 == 6:
                p()
        p()
        p(-1)
        p()
        p(-99)
        p()
        p()
        p(' {:7.3f}  ! Actual Model Scale submitted', scaling.SC)
        p(' {:>7}  ! Actual Open Water Test No.', openwater.id)
        p(' {:6.3f}  {:6.3f}-{:6.3f}  {:6.3f}  ! Actual rates. (full, ow, propuls.-test)',
          scaling.N['fs'], N_ow_min, N_ow_max, scaling.N['sp'])
        p(' ' + '{:^16}'*5,
          'T_tank', 'visc_kRP*10E6',
          'T_ow', 'visc_kOW*10E6',
          'visc_ks*10E6')
        p(' ' + '      {:4.1f}           {:6.4f}     '*2 + '     {:6.4f}',
          scaling.TEWA['sp'], viscosity_sp(scaling.TEWA['sp'])*1e6,
          scaling.TEWA['ow'], viscosity_ow(scaling.TEWA['ow'])*1e6,
          viscosity_fs(scaling.TEWA['fs'])*1e6
          )
        p()
        p(' Last parameter for Friction Line control: {}', None)
        p()
#        p(' _ This is VERSION No. {} of the new scaling method for '
#          'propellers of any design', __version__)
        p('   with the following friction lines:')
        p('   Open-Water      : {}'.format(friction_ow.name))
        p('   Self-Propulsion : {}'.format(friction_sp.name))
        p('   Full-Scale      : {}'.format(friction_fs.name))
