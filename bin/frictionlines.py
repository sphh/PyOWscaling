#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Output a table of friction lines to the standard output.

Command line parameters:
    A list of Reynolds numbers where the friction lines should be calculated
    at. If not supplied, the list stored in `RNs` is used.


Created on Thu Mar  2 10:23:09 2017

Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

__version__ = '3.1.0'


import sys
import glob

from owscaling import pluginframework, plugins


# Selected to have roughly equidistant spaces on a logarthmic scale
RNs = [1e3, 1.5e3, 2e3, 2.5e3, 3e3, 4e3, 6e3, 7.5e3,
       1e4, 1.5e4, 2e4, 2.5e4, 3e4, 4e4, 6e4, 7.5e4,
       1e5, 1.5e5, 2e5, 2.5e5, 3e5, 4e5, 6e5, 7.5e5,
       1e6, 1.5e6, 2e6, 2.5e6, 3e6, 4e6, 6e6, 7.5e6,
       1e7, 1.5e7, 2e7, 2.5e7, 3e7, 4e7, 6e7, 7.5e7,
       1e8, 1.5e8, 2e8, 2.5e8, 3e8, 4e8, 6e8, 7.5e8,
       1e9]
LCH_x = 1


if len(sys.argv) > 1:
    # We got some arguments. These should be a list of Reynolds numbers.
    RNs = [float(RN) for RN in sys.argv[1:]]

# Find all plug-ins
pluginmgr = pluginframework.PluginManager(
        plugin_dirs=glob.iglob('./plugins/**/', recursive=True))
pluginmgr.collect_plugins()

# Plug-ins implementing `IFrictionlinePlugin`
plugins_available = plugins.IFrictionlinePlugin.get_plugins()

# Initiate all available plug-ins
lines = {name: klass({})
         for name, klass in plugins_available.items()
         if name not in ('Constant', 'von-Kármán')}

# Add some lines with non-default arguments
nonstandardline = plugins.IFrictionlinePlugin.get_plugins()['Schlichting/Schulze']({})
nonstandardline.factor = 0.001

#
# Print a table with the values
#

# Header
print('# ', end='')
print('RN', end='')
for name in lines:
    print('\t', end='')
    print(name, end='')
print('\t', end='')
print('Sch/Sch')        # Lines with non-default arguments

for RN in RNs:
    print(RN, end='')
    for line in lines.values():
        try:
            cf = line(RN, LCH_x=LCH_x)
        except:
            cf = None
        print('\t', end='')
        print(cf, end='')
    print('\t', end='')
    # Lines with non-default arguments
    print(nonstandardline(RN, LCH_x=LCH_x), end='')
    print()
