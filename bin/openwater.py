#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Command line frontend to PyOWscaling.

This program provides a command line interface (CLI) to the PyOWscaling
framework for scaling open-water characteristics. It reads open-water data
from files and saves the scaled characteristics to output files. It enables
the user to select the plug-ins used for scaling on the command line by
specifying parameters, such as:

    $ openwater.py \
        --propeller HSVA docs/geo2188 docs/blp2188 \
        --openwater HSVA docs/ow2188 \
        --method ITTC \
        --viscosity-ow HSVA-Tank \
        --viscosity-sp HSVA-Tank \
        --viscosity-fs HSVA-Sea \
        --friction-ow ITTC-Model \
        --friction-sp ITTC-Model \
        --friction-fs ITTC-Ship \
        --formfactor ITTC \
        --output HSVA.ypp 2188.ypp \
        --output HSVA.csv 2188.csv \
        24 16 8 1.46

This call reads the propeller geometry files `docs/geo2188` and `docs/blb2188`
and the open-water data `docs/ow2188`, each in `HSVA`'s file format. It scales
the open-water characteristics accoding to the `ITTC` method. The viscosity
is calculated according to `HSVA` and the friction lines uses are `ITTC-Model`,
`ITTC-Model` and `ITTC-Ship` for open-water, self-propulsion and full scale
conditions, respectively. It also uses the `ITTC` formfactor. The scaled
open-water data is written to the files `2188.ypp` and `2188.csv` using the
`HSVA.ypp` and `HSVA.csv` formats, respectively. The positional parameters
24 16 8 and 1.46 are the model scale, the shaft revs during self-propulsion
test (in [Hz]), the water temperature during self-proplusion test (in [°C])
and the shaft revs for full scale (in [Hz]). An optional fifth positional
argument specifies the water temperature for full scale (in [°C]). If it is
not given, it defaults to 15°C.

Help can be obtained by calling

    $ openwater.py --help

This shows all possible and required parameters, but it also lists in the last
paragraph the possibilities how to set the scaling behavious. Let us take a
closer look at the scaling method itself. This uses the argument `--method`:

    $ openwater.py --method --help

This tells us the available scaling methods. Let us pick the ITTC method and
show its help message:

    $ openwater.py --method ITTC -h

If you want to know more about this method, use the long `--help` option:

    $ openwater.py --method ITTC --help

This will also show the method's documentation.

With this help-system it should be possible to drill down to any plug-in.

If you want to extend the features by writing your own plug-in, have a good
look at the documentation supplied.

Since this framework is published under the Lesser GNU General Public License
(LPGL), you can incorporate this framework into your program without paying
license fees. You can even use it in your commercial program, but have a good
look at the license below.


Created on Fri Jan 27 09:21:50 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

__version__ = '3.1.0'

import sys
import os.path
import argparse
import glob
from collections import OrderedDict

from owscaling import pluginframework, plugins, helpers
from owscaling.owscaling import OWScaling


# CONFIGURATION
__MIN_PYTHON_VERSION__ = (3, 5)         # Minimum required Python version
__COPYRIGHT__ = 'Copyright (C) 2017 Stephan Helma & (C) 2017 Stone Marine Propulsion Ltd'
PLUGIN_SYSTEMDIR = os.path.realpath(    # Directory of system plugins
        os.path.join(
                os.path.dirname(plugins.__file__),  # Path of plugin.py
                'plugins'))                         # Relative path to plugin.py

# Check minimum required Python version
if sys.version_info < __MIN_PYTHON_VERSION__:
    sys.exit(
            'Sorry, you have Python '
            '{version.major}.{version.minor}.{version.micro} installed, '
            'but you need at least Python {required}.'.format(
                    version=sys.version_info,
                    required='.'.join(map(str, __MIN_PYTHON_VERSION__))))


# Plug-ins which can be specified on the command line. The key is the option
# and the value the interface the plug-in must implement.
INTERFACES = OrderedDict([
        ('--method', plugins.IScalingPlugin),
        ('--friction-ow', plugins.IFrictionlinePlugin),
        ('--friction-sp', plugins.IFrictionlinePlugin),
        ('--friction-fs', plugins.IFrictionlinePlugin),
        ('--formfactor', plugins.IFormfactorPlugin),
        # XXX:
        #   Use density plugin?
        #   ('--density', plugins.IDensityPlugin),
        ('--viscosity-ow', plugins.IViscosityPlugin),
        ('--viscosity-sp', plugins.IViscosityPlugin),
        ('--viscosity-fs', plugins.IViscosityPlugin),
        ('--openwater', plugins.IOWReaderPlugin),
        ('--propeller', plugins.IPropellergeometryPlugin),
        ('--output', plugins.IOWWriterPlugin),
        ])


def get_main_argparser(interfaces=INTERFACES, partial=False):
    """Retrieve the parser for the main command line arguments.

    Parameters:
        interfaces:
            The dictionary with the plug-ins required. The keys are the long
            command line arguments selecting the plug-in (e.g. `'--method'`)
            and the values the corresponding interface class (e.g.
            `plugins.IScalingPlugin`).
        partial=False:
            If `True`, a partial parser will be returned. This partial parser
            does not handle help and positional parameters removed. (This can
            be used for initial parsing with `.parse_known_args()`.)

    Return:
        A tuple with the parser and the number of positional arguments
        required.
    """

    #
    # Parser
    #

    parser = helpers.OWArgumentParser(
            description='Scale an open-water test to self-propulsion and '
                        'full-scale Reynolds numbers.',
            epilog=__COPYRIGHT__,
            interfaces=interfaces,
            allow_abbrev=False,
            add_help=not partial)

    #
    # Positional parameters (only if full parser requested)
    #

    if not partial:
        # Model scale λ [-]
        parser.add_argument(
                'SC',
                type=helpers.LimitedFloat(min=0, endpoints=']]'),
                help='model scale λ [-]')

        # N_sp     [Hz]  Shaft revs during self-propulsion test
        parser.add_argument(
                'N_sp',
                type=helpers.LimitedFloat(min=0),
                help='shaft revolutions during self-propulsion test [Hz]')

        # TEWA_sp  [°C]  Temperature water during self-propulsion test
        parser.add_argument(
                'TEWA_sp',
                type=helpers.LimitedFloat(min=0, max=100),
                help='water temperature during self-propulsion test [°C]')

        # N_fs     [Hz]  Shaft revs for full scale
        parser.add_argument(
                'N_fs',
                type=helpers.LimitedFloat(min=0),
                help='shaft revolutions for full scale [Hz]')

        # TEWA_fs  [°C]  Temperature water during self-propulsion test
        parser.add_argument(
                'TEWA_fs',
                nargs='?', default=15,
                type=helpers.LimitedFloat(min=0, max=100),
                help='water temperature for full scale [°C] '
                     '(default: %(default)s)')

    #
    # General arguments
    #

    parser.add_argument(
            '-V', '--version',
            action='version',
            version='%(prog)s v{}'.format(__version__))

    parser.add_argument(
            '-d', '--debug',
            action='store_true',
            help='debug the main program')

    parser.add_argument(
            '-p', '--plugins',
            nargs='+',
            default=[],
            help='additional directories with plug-ins; '
                 'pathname pattern extension is available '
                 '(use ** for recursive directories, '
                 'you might have to put it into "" to suppress expansion by '
                 'the operating system)',
            metavar='PLUGINDIR')

    return parser

def split_argv(parser, interfaces=INTERFACES):
    """Parse the command line arguments.

    Parameters:
        interfaces:
            The dictionary with the plug-ins required. The keys are the long
            command line arguments selecting the plug-in (e.g. `'--method'`)
            and the values the corresponding interface class (e.g.
            `plugins.IScalingPlugin`).

    Return:
        A tuple with the list of main arguments and a dictionary with the
        plug-ins' arguments for every mentioning of the plug-in's long command
        line argument selecting it.
    """

    #
    # Find number of positional arguments
    #

    # The string 'positional arguments' might get translated in localized
    # installation of Python, so we have to tranlate it here as well using
    # the `argparse._()` translate function.
    no_params = 0
    _positional_arguments = argparse._('positional arguments')
    for g in parser._action_groups:
        if g.title == _positional_arguments:
            no_params = len(g._group_actions)
            break

    # Each plug-in has it's own independent command line parameters. We split
    # the command line parameters on these stop words and store the arguments
    # in the appropriate list; start with '--' for the main command line
    # arguments. This makes a directory with the plug-in keywords. The
    # elements are lists of lists with the options to the plug-in keywords.
    # The first call of a plug-in keyword creates the item in the dictionary
    # and adds the list of options, every further call adds another list to
    # the item. If the plug-in keyword only appears once, we get
    # `[[options,...]]` and if the plug-in keyword appears more than once,
    # we get `[[options,...], [options,...],...]
    argv = {'--': []}
    pluginargv = argv['--']
    for a in sys.argv[1:]:      # Argument list without command line parameters
        if a == '--':
            pluginargv = argv['--']
            continue
        elif a in interfaces:
            try:
                # Switch active receptor of command line arguments ...
                subargs = argv[a]
            except KeyError:
                # ... or add command line argument and empty list to the
                # `argv` dictionary
                argv[a] = []
                subargs = argv[a]
            # Append an empty list ...
            subargs.append([])
            # ... and make this the list receiving the arguments
            pluginargv = subargs[-1]
            continue
        pluginargv.append(a)

    # Now we have sorted the command line arguments and collected all options
    # for the arguments in the dictionary `argv`. There is still one task
    # left:
    # The last argument (currently references in `pluginargv`) contains all
    # positional arguments. We have to remove them and add them to the main
    # command line argument.
    args = []
    for i in range(no_params):
        # All positional arguments must be numerical!
        try:
            float(pluginargv[-1])
        except (IndexError, ValueError):
            break
        args.append(pluginargv.pop())

    # Reverse args, because we worked our way back to front
    args.reverse()

    # Store the command line positional parameters in the main parameter list,
    # but separate them by '--', otherwise '--plugins' could swallow up all
    # parameters
    if args:
        if argv['--'] and '--' not in argv['--']:
            argv['--'].append('--')
        argv['--'].extend(args)

    return argv

def display_subargs_help(subargv, plugins):
    to_exit = False
    for ps in subargv:
        if ps:
            plugin = plugins[ps[0]]
            interfacename = plugin[list(plugin.keys())[0]].interfacename
            print('Available {} plug-ins:\n'
                  '\t{}\n'
                  '\n'
                  'Use "{prog} {arg} PLUG-IN -h" to get help.'.format(
                          interfacename, '\n\t'.join(sorted(plugin.keys())),
                          arg=ps[0],
                          prog=os.path.basename(sys.argv[0])))
            to_exit = True

    if to_exit:
        sys.exit()


def parse_subargs(subargv, plugins):
    """Parse the plug-in's arguments and initialize the plug-ins.

    Parameters:
        subargv:
            A list of lists with the plug-in's arguments for every mentioning
            of the plug-in's long command line argument selecting it.
        plugins:
            The dictionary with the available plug-ins with one interface
            (selected by one command line arguments (e.g. `'--method'`)).

    Return:
        The list with the initialized plug-ins.
    """

    # By now we split our command line arguments into subsets for each plug-in
    # type. We also processed the general arguments and parameters. This
    # processing took also care of the general '--help' argument. We also
    # handled the '--help' argument coming straight after the plug-in
    # selection argument. A '-h' or '--help' argument anywhere else is passed
    # to the argument parser of the plug-in and handled there.
    # But we have to check, if the specified plug-in is available at all.
    interfacename = plugins[list(plugins.keys())[0]].interfacename
    loaded_plugins = []
    for argv in subargv:

        # The plug-in's name, used to select it
        name = argv.pop(0)

        # Check if plug-in is available
        if name not in plugins.keys():
            sys.exit('{} "{}" not available.\n'
                     'Available {} plug-ins:\n'
                     '\t{}'.format(
                            interfacename.capitalize(),
                            name,
                            interfacename,
                            '\n\t'.join(sorted(plugins.keys()))))

        # Call `__init__()` on our requested plugins
        loaded_plugins.append(plugins[name](argv=argv))

    return loaded_plugins

def help_missing_plugins(plugins_no, how_often='exactly one time'):
    if how_often and not how_often.startswith(' '):
        how_often = ' '+how_often
    sys.exit(
        "The following command line option(s) need(s) to be supplied{}:\n"
        "\t{}\n"
        "Call '{prog} --help' to get some help.".format(
                how_often,
                "\n\t".join(
                        sorted(["{:<16}({} times)".format(p, no)
                                for p, no in plugins_no.items()])),
                prog=os.path.basename(sys.argv[0])))

# =============================================================================

if __name__ == '__main__':

    #
    # Process the main command line
    #

    # Get two parser: One full parser (`parser`) and and one which does not
    # handle the help options and positional parameters (`partial_parser`)
    parser = get_main_argparser()
    partial_parser = get_main_argparser(partial=True)

    # Split the command line arguments into lists for every plug-in
    argv = split_argv(parser)

    # Handle the help option
    if '--help' in argv['--'] or '-h' in argv['--']:
        parser.parse_args(argv['--'])

    # Partially parse the command line
    partial_main_args = partial_parser.parse_known_args(argv['--'])[0]

    if partial_main_args.debug:
        print('Arguments')
        print('=========')
        print()
        print('Main:')
        print('    {}'.format(argv['--']))
        for opt, interface in INTERFACES.items():
            # We have to use `str(interface.interfacename)` because we insisted
            # to being `interface` a read-only `@property` and now Python
            # returns a `property` object and not a string - and a `property`
            # object does not have a `.capitalize()` method.
            print('{} ({}):'.format(
                    str(interface.interfacename).capitalize(), opt))
            if opt is None:
                print('    ✘')
            else:
                try:
                    print('    {}'.format(argv[opt]))
                except KeyError:
                    print('    ✘')
        print()

    #
    # Generate list of plug-in directories
    #

    plugin_dirs = []
    plugin_dirs_notfound = []

    # Directories supplied at the command line
    for d in partial_main_args.plugins:
        pdirs = [p for p in glob.iglob(d, recursive=True) if os.path.isdir(p)]
        if pdirs:
            plugin_dirs.extend(pdirs)
        else:
            plugin_dirs_notfound.append(d)

    # Directories below the systems plugin directory.
    # Note:
    # The empty string as last argument to `os.path.join()` ensures that we
    # get a `/` at the end of the path. The path will look like
    # `./plugins/**/`, but with the absolute path instead of `.`.
    # This final `/` ensures that we only get directories from the `iglob()`
    # call.
    plugin_systemdirs = glob.iglob(os.path.join(PLUGIN_SYSTEMDIR, '**', ''),
                                   recursive=True)
    plugin_dirs.extend(plugin_systemdirs)

    if partial_main_args.debug:
        print('Plug-in directories')
        print('===================')
        print()
        print('System:')
        if os.path.isdir(PLUGIN_SYSTEMDIR):
            print('  ✔ {}'.format(PLUGIN_SYSTEMDIR))
        else:
            print('  ✘ {}'.format(PLUGIN_SYSTEMDIR))
        print('Command line:')
        if partial_main_args.plugins:
            for d in partial_main_args.plugins:
                if d not in plugin_dirs_notfound:
                    print('  ✔ {}'.format(d))
                else:
                    print('  ✘ {}'.format(d))
        else:
            print('    [empty]')
        print()
        print('Finally:')
        for d in plugin_dirs:
            print('    {}'.format(d))
        print()

    #
    # Collect all plug-ins
    #

    pluginmgr = pluginframework.PluginManager(plugin_dirs=plugin_dirs)
    pluginmgr.collect_plugins()

    plugins_available = {
            key: interface.get_plugins()
            for key, interface in INTERFACES.items()}

    #
    # Check if we found at least one plug-in for all needed interfaces
    #

    plugins_notfound = [opt for opt, p in plugins_available.items() if not p]
    if plugins_notfound:
        sys.exit(
            "The plug-ins for the following command line option(s) "
                "could not be found:\n"
            "\t{}\n"
            "* Consider setting the plug-in directories with the "
                "'--plugins' option.\n"
            "* Call '{prog} --help' to get some help.\n"
            "* Call with '--debug' option to see more information why these"
                "plug-ins could not be found.".format(
                    "\n\t".join(sorted(plugins_notfound)),
                    prog=os.path.basename(sys.argv[0])))

    #
    # Show all plug-ins
    #

    if partial_main_args.debug:
        print('Available plug-ins')
        print('==================')
        print()
        for opt, interface in INTERFACES.items():
            # We have to use `str(interface.interfacename)` because we insisted
            # to being `interface` a read-only `@property` and now Python
            # returns a `property` object and not a string - and a `property`
            # object does not have a `.capitalize()` method.
            print('{} ({}):'.format(
                    str(interface.interfacename).capitalize(), opt))
            interface.print_plugins()
        print()

    #
    # Check if any of the plug-ins requested general help
    #

    display_subargs_help(
            [[opt
              for argv in argv[opt]
              if argv[0] in ('-h', '--help')
              ]
             for opt in INTERFACES
             if opt.startswith('--')
                 and opt in argv
                 and opt in plugins_available],
            plugins_available)

    #
    # Process the sub command lines
    #

    plugins_loaded = {
            opt: parse_subargs(argv[opt], plugins_available[opt])
            for opt in INTERFACES
            if opt.startswith('--')
                and opt in argv
                and opt in plugins_available}

    #
    # Process the main command line
    #

    main_args = parser.parse_args(argv['--'])

    #
    # Print what we know so far
    #

    if main_args.debug:
        print('Summary')
        print('=======')
        print()
        for opt, interface in INTERFACES.items():
            try:
                for p in plugins_loaded[opt]:
                    print(opt)
                    print('-'*len(opt))
                    print(p)
            except KeyError:
                print(interface.interfacename.capitalize())
                print('        none loaded')

        print('Conditions')
        print('==========')
        print()
        print('Scale:')
        print('        {}'.format(main_args.SC))
        print('Self-propulsion:')
        print('        Shaft speed:{} Hz'.format(main_args.N_sp))
        print('        Temperature:{}°C'.format(main_args.TEWA_sp))
        print('Full scale:')
        print('        Shaft speed:{} Hz'.format(main_args.N_fs))
        print('        Temperature:{}°C'.format(main_args.TEWA_fs))
        print()

    #
    # Check if we have all plugins
    #

    # "--output" option is different, because it can appear multiple times or
    # even not appearing at all (but what is the use of that?).
    try:
        outputs = plugins_loaded.pop('--output')
    except KeyError:
        help_missing_plugins({'--output': 0}, how_often='')

    # Collect all plug-ins specified on command line more than one time
    plugins_no = {opt: len(p)
                  for opt, p in plugins_loaded.items()
                  if len(p) != 1}
    # Extend by all plug-ins not specified on command line
    plugins_no.update({opt: 0
                       for opt in INTERFACES
                       if opt != '--output' and opt not in plugins_loaded})
    if plugins_no:
            sys.exit(
                "The following command line option(s) need(s) to be supplied "
                "exactly one time:\n"
                "\t{}\n"
                "Call '{prog} --help' to get some help.".format(
                        "\n\t".join(
                                sorted(["{:<16}({} times)".format(p, no)
                                        for p, no in plugins_no.items()])),
                        prog=os.path.basename(sys.argv[0])))

    #
    # Initialize plug-ins further
    #

    # Scaling plug-in initialization needs all other plug-ins! Let us remove it
    # from our list of loaded plug-ins and store it for later.
    scaling = plugins_loaded.pop('--method')[0]

    # We ensured that we got just one option, so let us flatten the list!
    plugins_loaded = {opt: p[0] for opt, p in plugins_loaded.items()}

    #
    # Scale the open-water test
    #

    owscaling = OWScaling(
            scaling,
            propeller = plugins_loaded['--propeller'],
            openwater = plugins_loaded['--openwater'],
            viscosities = (plugins_loaded['--viscosity-ow'],
                           plugins_loaded['--viscosity-sp'],
                           plugins_loaded['--viscosity-fs']),
            # XXX:
            # Remove/Use density plug-in?
            densities = None,   # plugins_loaded['--density'],
            frictions = (plugins_loaded['--friction-ow'],
                         plugins_loaded['--friction-sp'],
                         plugins_loaded['--friction-fs']),
            formfactor = plugins_loaded['--formfactor'])
    owscaling.scale(
            main_args.SC,
            (main_args.N_sp, main_args.N_fs),
            (main_args.TEWA_sp, main_args.TEWA_fs))
    owscaling.outputs = outputs
    owscaling.write()
