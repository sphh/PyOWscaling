# PyOWscaling

> :warning: **This repository has been moved to https://codeberg.org/SMP/PyOWscaling/**

## Objectives
----------
A framework to scale propeller characteristics (open-water curves) measured at model scale to different Reynolds number regimes. This is required for the the power prediction of the trials when model testing ships.

The aim was not to write the fastest code, but to create a framework, which is easily extendable and lends itself to experimentation with different aspects of the scaling methods. That does not mean that it cannot be used for “production”.


## Documentation

The documentation can be found in the [PyOWscaling wiki](https://gitlab.com/sphh/PyOWscaling/wikis).


## License

The software is released under the [GNU Lesser General Public License](https://choosealicense.com/licenses/lgpl-3.0/).


## Bug report

Please report bugs at [PyOWscaling/issues](https://gitlab.com/sphh/PyOWscaling/issues).


## Changelog

## v3.1.1 (2017-09-19)

Documentation improvement.

## v3.1.0 (2017-09-19)

Added native stripe method and three new friction lines: CEHIPAR, Prandtl and
Blasius.

### v3.0.0 (2017-08-08)

Rework of the initialization API.

#### Warning
This release breaks with the previous API and requires a rework of your
plug-ins. This is a simple rework, but a rework nonetheless. Unfortunately
this became necessary to unify and simplify the parameters used in
initializing the plug-ins.

#### Downloads

- [Source code (tar.bz2)](https://gitlab.com/sphh/PyOWscaling/releases/owscaling-3.0.0.tar.bz2)

### v2.0.0 (2017-07-07)

#### Downloads

- [Source code (tar.bz2)](https://gitlab.com/sphh/PyOWscaling/releases/owscaling-2.0.0.tar.bz2)
